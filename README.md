<a href="https://git.io/typing-svg"><img src="https://readme-typing-svg.demolab.com?font=Fira+Code&size=22&duration=2000&pause=500&color=05A703&center=true&width=600&lines=Hi+there%2C+thanks+for+visit+my+gitlab;This+is+Benomas;An+experienced+and+apationated;+Fullstack+developer;Php+(laravel)+-+Javascript+(vue)+;Are+my+main+stack" alt="Typing SVG" /></a>

-----

<div align='center' style="display: flex; flex-wrap: wrap; justify-content: center; align-items: flex-start; column-gap: 20px;">

## Those technologies are were I have more experience

### For Backend
-----

<div align='left' style="display: flex; flex-wrap: wrap; justify-content: center; align-items: flex-start; column-gap: 20px;">

![](https://img.shields.io/badge/php---?style=for-the-badge&logo=php&color=313131&labelColor=313131&logoWidth=40)
![](https://img.shields.io/badge/laravel---?style=for-the-badge&logo=laravel&color=F03E30&labelColor=313131&logoWidth=40)
![](https://img.shields.io/badge/codeigniter---?style=for-the-badge&logo=codeigniter&color=C9340A&labelColor=313131&logoWidth=40)
![](https://img.shields.io/badge/wordpress---?style=for-the-badge&logo=wordpress&color=38677B&labelColor=313131&logoWidth=40)

</div>

### For Frontend
-----

<div align='left' style="display: flex; flex-wrap: wrap; justify-content: center; align-items: flex-start; column-gap: 20px;">

![](https://img.shields.io/badge/javascript---?style=for-the-badge&logo=javascript&color=B855BF&labelColor=B855BF&logoWidth=40)
![](https://img.shields.io/badge/jquery---?style=for-the-badge&logo=jquery&color=0769AD&labelColor=B855BF&logoWidth=40)
![](https://img.shields.io/badge/vue---?style=for-the-badge&logo=vue.js&color=3C7168&labelColor=B855BF&logoWidth=40)
![](https://img.shields.io/badge/angular---?style=for-the-badge&logo=angular&color=DD0031&labelColor=B855BF&logoWidth=40)

</div>

### For Databases
-----

<div align='left' style="display: flex; flex-wrap: wrap; justify-content: center; align-items: flex-start; column-gap: 20px;">

![](https://img.shields.io/badge/sql---?style=for-the-badge&logo=sql&color=3C4043&labelColor=313131&logoWidth=40)
![](https://img.shields.io/badge/mysql---?style=for-the-badge&logo=mysql&color=007B91&labelColor=3C4043&logoWidth=40)
![](https://img.shields.io/badge/mariadb---?style=for-the-badge&logo=mariadb&color=C0765A&labelColor=3C4043&logoWidth=40)
![](https://img.shields.io/badge/sqlserver---?style=for-the-badge&logo=microsoftsqlserver&color=E81917&labelColor=3C4043&logoWidth=40)
![](https://img.shields.io/badge/postgresql---?style=for-the-badge&logo=postgresql&color=336791&labelColor=3C4043&logoWidth=40)

</div>
</div>


